var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var mongoose = require("mongoose");

var dbUrl = "mongodb://kfir:1303@ds143774.mlab.com:43774/learning-node";

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

mongoose.Promise = Promise;

var Message = mongoose.model("Message", {
  name: String,
  message: String
});
var messages = [];

app.get("/messages", (req, res) => {
  Message.find({}, (err, messages) => {
    res.send(messages);
  });
});

app.post("/messages", (req, res) => {
  var message = new Message(req.body);

  message
    .save()
    .then(() => {
      return Message.find({ message: "badword" });
    })
    .then(censored => {
      if (censored) {
        return Message.remove({ _id: censored.id });
      }
      io.emit("message", req.body);
      res.sendStatus(200);
    })
    .catch(err => {
      res.sendStatus(500);
      return console.error(err);
    });
});

io.on("connection", socket => {
  console.log("user is connected");
});
mongoose.connect(dbUrl, { useMongoClient: true }, err => {
  console.log("mongo db connection", err);
});
http.listen("5000");
